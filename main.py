#!/usr/bin/env python3

import sys
import ec2

def dry_run():
    dry_run = False
    if len(sys.argv) == 2:
        if sys.argv[1] == "dry_run":
            dry_run = True
    return dry_run

new_ec2 = ec2.ec2_env()
if not dry_run:
    new_ec2.main()
