import boto3
from botocore.exceptions import ClientError
import os
import stat
import sys

session = boto3.session.Session()

class ec2_env:
    def __init__(self, ssh_name="test", sg_name="ssh_sg", ssh_filename="ec2-keypair.pem"):
        self.ec2 = session.resource('ec2')
        self.ec2_c = session.client('ec2')
        self.ssh_name=ssh_name
        self.ssh_filename=ssh_filename
        self.sg_name=sg_name
        self.get_vpc()

    def create_ssh_key(self):
        try:
            print("Creation of ssh key")
            key_pair = self.ec2.create_key_pair(KeyName=self.ssh_name)
            KeyPairOut = str(key_pair.key_material)
            outfile = open(self.ssh_filename, 'w')
            outfile.write(KeyPairOut)
        except ClientError:
            print("Ssh key already existed")
        #os.chmod(self.ssh_filename, stat.S_IRUSR | stat.S_IWUSR)

    def get_vpc(self):
        try:
            response = self.ec2_c.describe_vpcs()
            self.vpc_id = response.get('Vpcs', [{}])[0].get('VpcId', '')
        except ClientError as e:
            if e.response['Error']['Code'] == 'AuthFailure':
                print("Authentication problem")
            sys.exit(1)

    def create_sg(self, name="ssh_sg"):
        try:
            print("Creation of security group: allow ssh to all")
            response = self.ec2_c.create_security_group(GroupName=self.sg_name,
                                                Description='ssh',
                                                VpcId=self.vpc_id)
            self.security_group_id = response['GroupId']
            return self.ec2_c.authorize_security_group_ingress(
                GroupId=self.security_group_id,
                IpPermissions=[
                    {'IpProtocol': 'tcp',
                     'FromPort': 22,
                     'ToPort': 22,
                     'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}
                ])
        except ClientError:
            print("Security group already existed")

    def create_ec2(self):
        print('Creation of EC2 instance')
        instances = self.ec2.create_instances(
             ImageId='ami-08f3d892de259504d',
             MinCount=1,
             MaxCount=1,
             InstanceType='t2.micro',
             SecurityGroups=[self.sg_name],
             KeyName=self.ssh_name,
         )
        return instances[0].id

    def wait_ec2(self, ec2_id):
        waiter = self.ec2_c.get_waiter("instance_running")
        print('Instance is in creation')
        waiter.wait(InstanceIds=[ec2_id])
        response = self.ec2_c.describe_instances(
            InstanceIds=[
                ec2_id,
            ],
        )
        public_ip = response['Reservations'][0]['Instances'][0]['PublicIpAddress']
        print(f'Instance is ready ({ec2_id}, ip={public_ip}')
        print(f'ssh -i {self.ssh_filename} ec2-user@{public_ip}')

    def main(self):
        self.create_ssh_key()
        self.create_sg()
        ec2_id = self.create_ec2()
        self.wait_ec2(ec2_id)


